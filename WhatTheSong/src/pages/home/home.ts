import { StreamingMedia, StreamingAudioOptions, StreamingVideoOptions } from '@ionic-native/streaming-media';
import { Component } from '@angular/core';
// import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private  streamingMedia:StreamingMedia) {

  }


  startVideo(){
    let options: StreamingVideoOptions = {
      successCallback:() => {console.log('Video played')},
      errorCallback:() => {console.log('Error streaming')},
      orientation: 'portrait'
    }
    this.streamingMedia.playVideo('https://drive.google.com/file/d/1xJZkH6WW21N0LQImJiYZALixJQcWNOGi/view?usp=sharing', options);
  }
  startAudio(){
    let options: StreamingAudioOptions = {
      successCallback:() => {console.log('Video played')},
      errorCallback:() => {console.log('Error streaming')},
      initFullscreen: false,
    }
    this.streamingMedia.playAudio('https://sample-videos.com/audio/mp3/crowd-cheering.mp3', options);
    
  }
  stopAudio(){
    this.streamingMedia.stopAudio();
  }

}
